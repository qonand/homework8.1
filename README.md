# Prepare data

```sql
CREATE TABLE wallets (
    user_id bigint UNSIGNED NOT NULL PRIMARY KEY,
    balance DECIMAL (10,2) UNSIGNED NOT NULL
);

INSERT INTO wallets (user_id, balance) VALUES (1, 0);
```

# Check "Lost update" problem
### For READ UNCOMMITTED level

```sql
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 20;
    SELECT SLEEP(30);
COMMIT;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 10;
    SELECT SLEEP(30);
COMMIT;

SELECT balance FROM wallets WHERE user_id = 1; -- result 30, there is not the problem
```
### For READ COMMITTED level
```sql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 20;
    SELECT SLEEP(30);
COMMIT;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 10;
    SELECT SLEEP(30);
COMMIT;

SELECT balance FROM wallets WHERE user_id = 1; -- result 60, there is not the problem
```
### For REPEATABLE READ level
```sql
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 20;
    SELECT SLEEP(30);
COMMIT;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 10;
    SELECT SLEEP(30);
COMMIT;

SELECT balance FROM wallets WHERE user_id = 1; -- result 90, there is not the problem
```

### For SERIALIZABLE level
```sql
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 20;
    SELECT SLEEP(30);
COMMIT;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 10;
    SELECT SLEEP(30);
COMMIT;

SELECT balance FROM wallets WHERE user_id = 1; -- result 120, there is not the problem
```
# Check "dirty read" problem
### For READ UNCOMMITTED level
```sql
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 20;
    SELECT SLEEP(30);
ROLLBACK;

START TRANSACTION;
    SELECT * FROM wallets WHERE user_id = 1; -- result 140, there is the problem
    SELECT SLEEP(30);
COMMIT;
```

### For READ COMMITTED level
```sql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 20;
    SELECT SLEEP(30);
ROLLBACK;

START TRANSACTION;
    SELECT * FROM wallets WHERE user_id = 1; -- result 120, there is not the problem
    SELECT SLEEP(30);
COMMIT;
```
### For REPEATABLE READ level
```sql
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 20;
    SELECT SLEEP(30);
ROLLBACK;

START TRANSACTION;
    SELECT * FROM wallets WHERE user_id = 1; -- result 120, there is not the problem
    SELECT SLEEP(30);
COMMIT;
```
### For SERIALIZABLE level
```sql
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

START TRANSACTION;
    UPDATE wallets SET balance = balance + 20;
    SELECT SLEEP(30);
ROLLBACK;

START TRANSACTION;
    SELECT * FROM wallets WHERE user_id = 1; -- result 120, there is not the problem
    SELECT SLEEP(30);
COMMIT;
```
# Check "non-repeatable read" problem
### For READ UNCOMMITTED level
```sql
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

START TRANSACTION;
    SELECT SLEEP(30);
    UPDATE wallets SET balance = balance + 20;
COMMIT;

START TRANSACTION;
    SELECT * FROM wallets WHERE user_id = 1; -- result 120, 
    SELECT SLEEP(30);
    SELECT * FROM wallets WHERE user_id = 1; -- result 140, there is the problem
COMMIT;
```
### For READ COMMITTED level
```sql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

START TRANSACTION;
    SELECT SLEEP(30);
    UPDATE wallets SET balance = balance + 20;
COMMIT;

START TRANSACTION;
    SELECT * FROM wallets WHERE user_id = 1; -- result 140, 
    SELECT SLEEP(30);
    SELECT * FROM wallets WHERE user_id = 1; -- result 160, there is the problem
COMMIT;
```
### For REPEATABLE READ level
```sql
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;

START TRANSACTION;
    SELECT SLEEP(30);
    UPDATE wallets SET balance = balance + 20;    
COMMIT;

START TRANSACTION;
    SELECT * FROM wallets WHERE user_id = 1; -- result 160, 
    SELECT SLEEP(30);
    SELECT * FROM wallets WHERE user_id = 1; -- result 160, there is not the problem
COMMIT;
```
### For SERIALIZABLE level
```sql
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

START TRANSACTION;
    SELECT SLEEP(30);
    UPDATE wallets SET balance = balance + 20;
COMMIT;

START TRANSACTION;
    SELECT * FROM wallets WHERE user_id = 1; -- result 180, 
    SELECT SLEEP(30);
    SELECT * FROM wallets WHERE user_id = 1; -- result 180, there is not the problem
COMMIT;
```
# Check "phantom reads" problem
### For READ UNCOMMITTED level
```sql
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

START TRANSACTION;
    SELECT SLEEP(30);
    INSERT INTO wallets (user_id, balance) VALUES (2, 100);
COMMIT;

START TRANSACTION;
    SELECT sum(balance) FROM wallets; -- result 180
    SELECT SLEEP(30);
    SELECT sum(balance) FROM wallets; -- result 280, there is the problem
COMMIT;
```
### For READ COMMITTED level
```sql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

START TRANSACTION;
    SELECT SLEEP(30);
    INSERT INTO wallets (user_id, balance) VALUES (3, 100);
COMMIT;

START TRANSACTION;
    SELECT sum(balance) FROM wallets; -- result 280
    SELECT SLEEP(30);
    SELECT sum(balance) FROM wallets; -- result 380, the is the problem
COMMIT;
```
### For REPEATABLE READ level
```sql
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;

START TRANSACTION;
    SELECT SLEEP(30);
    INSERT INTO wallets (user_id, balance) VALUES (4, 100);
COMMIT;

START TRANSACTION;
    SELECT sum(balance) FROM wallets; -- result 380
    SELECT SLEEP(30);
    SELECT sum(balance) FROM wallets; -- result 380, the is not the problem
COMMIT;
```
### For SERIALIZABLE level
```sql
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

START TRANSACTION;
    SELECT SLEEP(30);
    INSERT INTO wallets (user_id, balance) VALUES (5, 100);
COMMIT;

START TRANSACTION;
    SELECT sum(balance) FROM wallets; -- result 580
    SELECT SLEEP(30);
    SELECT sum(balance) FROM wallets; -- result 580, the is not the problem
COMMIT;
```